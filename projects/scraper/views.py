import glob
import json
import os

from flask import jsonify, request, redirect, url_for, render_template
from flask.blueprints import Blueprint

from projects.scraper.scrapers.steam import get_detail, get_all_link, data_product

# blueprint config
scraper_blueprint = Blueprint('scraper', __name__, template_folder='templates', static_folder='static')


# route
@scraper_blueprint.route('/', methods=['GET', 'POST'])
def index():
    keywords = request.form.get('keywords')
    if request.method == 'POST':
        all_product = get_all_link(keywords=keywords)
        for product in all_product:
            products = get_detail(product)
            data_product.append(products)

        print("Total Data Product {}".format(len(data_product)))

        # writing json
        # write json
        results_dir = os.path.dirname(os.path.realpath(__file__))
        with open(f'{os.path.join(results_dir, "scrapers/results")}/{keywords.replace("+", " ")}.json', 'w+') as outfile:
            json.dump(data_product, outfile)

        return redirect('/api')
    else:
        return render_template('index.html')


# api
@scraper_blueprint.route('/api')
def api():
    datas = []
    json_dir = os.path.dirname(os.path.realpath(__file__))
    file_results = sorted(glob.glob(os.path.join(json_dir, 'scrapers/results/*.json')))
    for file in file_results:
        print('Reading File: {}'.format(file))
        with open(file, 'r') as json_file:
            json_data = json.load(json_file)
        datas += json_data
    return jsonify(games=datas)
