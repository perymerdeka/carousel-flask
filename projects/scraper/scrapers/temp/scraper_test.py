import requests
from bs4 import BeautifulSoup

headers = {
    'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36'
}

url = 'https://store.steampowered.com/'
res = requests.get(url, headers=headers)
soup = BeautifulSoup(res.text, 'html.parser')

# checker
f = open('res.html', 'w+')
f.write(res.text)
f.close()

# scraping process
contents = soup.find_all('a', attrs={'class':'tab_item'})
for content in contents:
    title = content.find('div', attrs={'class':'tab_item_name'}).text.strip()
    link = content.get('href')
    try:
        price = content.find('div', attrs={'class':'discount_final_price'}).text
    except:
        pass

    category = content.find('div', attrs={'class':'tab_item_top_tags'}).find('span', attrs={'class':'top_tag'}).text

    data_dict = {
        'title': title,
        'link': link,
        'price': price,
        'category': category
    }

    print(data_dict)