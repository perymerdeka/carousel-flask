import requests
from bs4 import BeautifulSoup
import json
import os

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36'
}

index = 0
data_product = []

def get_all_link(keywords):
    params = {
        'term': keywords
    }
    url = 'https://store.steampowered.com/'
    res = requests.get(url,params=params, headers=headers)
    soup = BeautifulSoup(res.text, 'html.parser')

    # checker
    temp_file = os.path.dirname(os.path.realpath(__file__))

    f = open(os.path.join(temp_file, 'temp/res.html'), 'w+')
    f.write(res.text)
    f.close()

    # scraping process
    results = []
    contents = soup.find_all('a', attrs={'class': 'tab_item'})
    for content in contents:
        title = content.find('div', attrs={'class': 'tab_item_name'}).text.strip()
        link = content.get('href')
        try:
            price = content.find('div', attrs={'class': 'discount_final_price'}).text
        except:
            pass

        category = content.find('div', attrs={'class': 'tab_item_top_tags'}).find('span',
                                                                                  attrs={'class': 'top_tag'}).text
        data_dict = {
            'title': title,
            'link': link,
            'price': price,
            'category': category
        }

        results.append(data_dict)

    print(f'Item Found In Pages: {len(results)}')
    return results


def get_detail(results):
    # dev mode
    # url = 'https://store.steampowered.com/app/945360/Among_Us/'
    global index
    index +=1
    url = results['link']
    res = requests.get(url, headers=headers)
    print('Getting Detail: {} . Stored Item:  {}'.format(url, index))
    soup = BeautifulSoup(res.text, 'html.parser')
    try:
        image = soup.find('img', attrs={'class': 'game_header_image_full'})['src']
    except:
        try:
            image = soup.find('div', attrs={'class':'img_ctn'}).find('img')['src']
        except:
            image = soup.find('img', attrs={'class':'package_header'})['src']
    try:
        release_date = soup.find('div', attrs={'class': 'release_date'}).find('div', attrs={'class': 'date'}).text
    except:
        release_date = 'No Release Date'

    results['image'] = image
    results['release date'] = release_date

    return results

def run():
    global data_product
    keywords = input('Enter Keywords: ')
    keywords = keywords.replace(' ', '+')

    all_product = get_all_link(keywords=keywords)
    for product in all_product:
        products = get_detail(product)
        data_product.append(products)

    print("Total Data Product {}".format(len(data_product)))

    # write json
    with open(f'results/{keywords.replace("+", " ")}.json', 'w+') as outfile:
        json.dump(data_product, outfile)

    # read json
    with open(f'results/{keywords.replace("+", " ")}.json', 'r') as json_file:
        data_product = json.load(json_file)