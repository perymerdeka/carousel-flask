from flask import Flask

def create_app(script_info=None):
    from projects.scraper.views import scraper_blueprint


    app = Flask(__name__, static_url_path='')

    # register blueprint
    app.register_blueprint(scraper_blueprint)

    return app