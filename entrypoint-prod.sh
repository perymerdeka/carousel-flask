#!/bin/sh
flask db upgrade
flask run -h 0.0.0.0
gunicorn --bind :9999 -w 4 manage:app