# Steam Simple Scraper

---

### Run Scraper Non Dockerized

* **Create Virtualenv**

```bash
python -m venv venv
```

* **Activate Virtualenv on Linux/Mac**

```bash
source venv/bin/activate
```

* **Install Requirements**

```bash
pip install -r requirements.txt --upgrade pip
```

* **Run Scraper**

```bash
source ./scripts/run_dev.sh
```

